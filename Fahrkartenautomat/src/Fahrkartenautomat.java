﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag; 
    	double rückgabebetrag;
    	boolean stop = false;
    	do {
    		zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    		// Geldeinwurf
    		// -----------------
    		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben(8,"\nFahrschein wird ausgegeben");    
       
    		// Rückgeldberechnung und -Ausgabe
    		// -------------------------------
    		rueckgeldAusgeben(rückgabebetrag);
    	}while(!stop);
       
    }


	


	private static double fahrkartenbestellungErfassen() {
		fahrkartenAusgeben(25, "Fahrkartenbestellvorgang:");
		Scanner tastatur = new Scanner(System.in);
    	String[] typ = new String[] {
    			"Einzelfahrschein Berlin AB",
    			"Einzelfahrschein Berlin BC",
    			"Einzelfahrschein Berlin ABC",
    			"Kurzstrecke",
    			"Tageskarte Berlin AB",
    			"Tageskarte Berlin BC",
    			"Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB",
    			"Kleingruppen-Tageskarte Berlin BC",
    			"Kleingruppen-Tageskarte Berlin ABC"
    	};
    	double[] preis = new double[] {2.9,3.3,3.6,1.9,8.6,9.0,9.6,23.5,24.3,24.9};
    	int auswahl;
	    double zahlen = 0.0;
	    double zahlenGesamt = 0.0;
	    boolean halt = false;
	    do {
	    	System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
	    	for(int i = 0; i<typ.length; i++){
	    		System.out.printf("%s%s%.2f%s%d%s", typ[i]," [",preis[i]," EUR] (",(i+1),")\n");
	    	}
	    	System.out.print("Zum bezahlen gehen ("+(typ.length+1)+")\n");
	    	auswahl = tastatur.nextInt();
	    	if (auswahl<typ.length+1 && auswahl>0) {
	    		zahlen = preis[auswahl-1] * anzahlAngeben(tastatur);
	    		zahlenGesamt += zahlen;
	    	}
	    	else if(auswahl==typ.length+1) {
	    		halt = true;
	    	}
	    	else {
	    		System.out.println("Bitte eine gültige Fahrkarte wählen.");
	    	}
	    }while(!halt);
		return zahlenGesamt;
	}
	private static double fahrkartenBezahlen(double zahlen) {
	    double gezahlt = 0.0;
	    Scanner tastatur = new Scanner(System.in);
		while(gezahlt < zahlen)
	       {
	    	   System.out.printf("%s%.2f%s", "Noch zu zahlen: ",(zahlen - gezahlt), " Euro");
	    	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2,00 Euro): ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	           gezahlt += eingeworfeneMünze;
	       }

		double rueck = gezahlt - zahlen;
		return rueck;
	}
	private static void fahrkartenAusgeben(int limit, String text) {
		System.out.println(text);
	    for ( int i = 0; i < limit; i++)
	    {
	    System.out.print("=");
	    warte(250);
	       }
	       System.out.println("\n\n");	
	}
	private static void rueckgeldAusgeben(double rueck) {
		String einheit = "";
		int a = (int)Math.round(rueck*100); //Rundungsfehler bei float und double
	    if(a > 0)
	       {
	    	System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rueck, " Euro");
	    	System.out.println("wird in folgenden Münzen ausgezahlt: ");
	    	muenzeAusgabe(a,einheit);
	        
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n");
		
	}
	private static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
			}
		catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	private static void muenzeAusgabe(int betrag, String einheit) {
		while(betrag >= 200) // 2 EURO-Münzen
        {
			einheit = "Euro";
        	System.out.println("2,00 "+einheit);
        	betrag -= 200;
        }
        while(betrag >= 100) // 1 EURO-Münzen
        {	
        	einheit = "Euro";
        	System.out.println("1,00 "+einheit);
        	betrag -= 100;
        }
        while(betrag >= 50) // 50 CENT-Münzen
        {
        	einheit = "Cent";
        	System.out.println("50 "+einheit);
        	betrag -= 50;
        }
        while(betrag >= 0.2) // 20 CENT-Münzen
        {
        	einheit = "Cent";
        	System.out.println("20 "+einheit);
        	betrag -= 20;  
        }
        while(betrag >= 10) // 10 CENT-Münzen
        {
        	einheit = "Cent";
        	System.out.println("10 "+einheit);
        	betrag -= 10;
        }
        while(betrag >= 5)// 5 CENT-Münzen
        {
        	einheit = "Cent";
        	System.out.println("5 "+einheit);
        	betrag -= 5;
        }
	}
	private static double anzahlAngeben(Scanner tastatur) {
	    System.out.print("Anzahl Tickets zwischen 1 und 10: ");
	    double anzahl = tastatur.nextInt();
	    if(anzahl<1 || anzahl >10) {
	    	anzahl = 0;
	    	System.out.println("Es können nur zwischen 1 bis 10 Tickets bestellt werden.");
	    }
	    return anzahl;
	}
}